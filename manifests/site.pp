class migration::globals (
  $global_home = "/home/$id",
  $global_private_source ="/home/$id/backup/private",
  $global_base = "/not/set",
  $global_user_cache = "/home/$id/.cache",
  $global_user_fast = "/home/$id/.fast"
) {
  file { "$global_user_cache":
    ensure => directory,
  }
  file { "$global_user_fast":
    ensure => directory,
  }
}

class common {
  include migration::globals
  include cache_dirs
  include powerline_fonts
}

class my_workstation {
  class { 'migration::globals':
    global_home => "/home/$id",
    global_private_source => "/home/$id/backup/private",
    global_user_cache => "/home/$id/.cache",
    global_user_fast => "/home/$id/.fast"
  }

  include common
  include vim_bundles
}

# vagrant instance
node database {
  class { 'migration::globals':
    global_home => "/home/$id",
    global_private_source => "/home/$id/backup/private",
    global_user_cache => "/home/$id/.cache",
    global_user_fast => "/home/$id/.fast"
  }

  include common
}

node 'czarek-M85M-US2H' {
  include my_workstation
}

node 'cezary-pc' {
  include my_workstation
}

node 'cezary-nitro-an517-41.' {
  include my_workstation
}

node 'cezary-nitro-an517-41.home' {
  include my_workstation
}

node 'cezary-linuxonmac.home' {
  include my_workstation
}

class docker_common {
  class { 'migration::globals':
    global_home => "/home/$id",
    global_private_source => "/home/$id/backup/private",
    global_user_cache => "/home/$id/.cache",
    global_user_fast => "/home/$id/.fast"
  }
  include common
  include vim_bundles
}

# Default for docker recovery/development machines
node /^[a-z0-9]{12}\.[a-z0-9]+$/ { include docker_common }
node /^[a-z0-9]{12}$/ { include docker_common }
