#!/bin/sh

[ "$SHELL" = "/bin/sh" ] && { echo "error: sorry, sh is not supported - try setting default account shell to ZSH" >&2; exit 1; }


GREP=/bin/grep
[ -e "$GREP" ] || GREP="/usr/bin/grep"


do_update_workstation()
{
  verify_workstation_update_file || setup_workstation_update_file && { verify_workstation_update_file || exit 2; }
  . "${update_workstation_file}"
  update_workstation
}

setup_zsh()
{
  do_update_workstation
}

verify_zsh()
{
  [ -e ~/.zshrc ] || { echo "ZSH not set up" >&2; return 1;  }
}

setup_vim()
{
  do_update_workstation
}

verify_vim()
{
  [ -h ~/.vim/autoload/pathogen.vim ] || { echo "VIM not set up" >&2; return 1; }
}

verify_head()
{
  [ -n "${FAIL_ONCE_FOR_REMOTE_MAKE:-}" ] && return 0
  # We fail on first call so setup_head is called when we're using make,
  # so we can call make multiple times
  FAIL_ONCE_FOR_REMOTE_MAKE=1

  oldpwd="$PWD"
  cd "$HOME/workspace/czarek_workstation"
  if ! git status --short | "$GREP" -E "^ M "; then
    RES=0
  else
    echo "Checkout dirty, assuming called remotely after rsync."
    RES=1
  fi
  cd "$oldpwd"
  return $RES
}

setup_head()
{
  do_update_workstation
}

verify_zsh || setup_zsh && { verify_zsh || exit 1; }
verify_vim || setup_vim && { verify_vim || exit 1; }
verify_head || setup_head && {verify_head || exit 1; }
