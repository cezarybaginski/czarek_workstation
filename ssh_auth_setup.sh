#!/bin/sh

. ./settings.mk

user_and_host="${USER_TO_SETUP}@${HOST_TO_SETUP}"

pubkey=~/.ssh/id_rsa.pub

cat $pubkey | ssh -p "${HOST_SSH_PORT}" "$user_and_host" sh -c 'echo "adding to authorized_keys" && mkdir -p ${HOME}/.ssh && chmod 0700 ${HOME}/.ssh && tee -a ~/.ssh/authorized_keys'
