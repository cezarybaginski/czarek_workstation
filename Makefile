SETTINGS_FILE=settings.mk
include $(SETTINGS_FILE)

SRC_RSYNC_DIR=${PWD}
DST_DIR=$(subst /home/${USER},/home/${USER_TO_SETUP},${PWD})
DST_RSYNC_DIR=$(USER_AND_HOST):$(DST_DIR)

SCRIPTFILE=setup.sh
USER_AND_HOST=$(USER_TO_SETUP)@$(HOST_TO_SETUP)

update: $(SCRIPTFILE).sent

DEVMODE_RSYNC_FILES=manifest modules lib/symlinking lib/setup_functions lib/update_workstation $(SETTINGS_FILE)

BOOTSTRAP_FILE_SEQUENCE=settings.mk lib/setup_functions bootstrap/bootstrap.sh
UPDATE_FILE_SEQUENCE=settings.mk lib/setup_functions setup.sh

$(SCRIPTFILE).sent : $(SCRIPTFILE)
	cat $(BOOTSTRAP_FILE_SEQUENCE) | ssh -A -X -t -p $(HOST_SSH_PORT) $(USER_AND_HOST)
	rsync -aR -e 'ssh -p $(HOST_SSH_PORT)' --port=$(HOST_SSH_PORT) $(DEVMODE_RSYNC_FILES) $(DST_RSYNC_DIR)/
	cat $(UPDATE_FILE_SEQUENCE) | ssh -A -X -t -p $(HOST_SSH_PORT) $(USER_AND_HOST)
	ssh -A -X -t -p $(HOST_SSH_PORT) $(USER_AND_HOST) puppet apply --modulepath=$(DST_DIR)/modules $(DST_DIR)/manifest/all.pp

# vi: noet ts=2 sw=2:
