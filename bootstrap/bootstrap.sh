#!/bin/sh

set -eu

verify_workstation_update_file || setup_workstation_update_file && { verify_workstation_update_file || exit 2; }
