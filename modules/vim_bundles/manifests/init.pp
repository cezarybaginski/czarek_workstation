# TODO: handle repositories that have moved?

# TODO: parametize (user home)
class vim_bundles {
  $repos = [
    'Shougo/unite.vim',
    'altercation/vim-colors-solarized',
    'bling/vim-airline',
    'vimoutliner/vimoutliner',
    'editorconfig/editorconfig-vim',
    'edkolev/tmuxline.vim',
    'groenewege/vim-less',
    'guns/xterm-color-table.vim',
    'hail2u/vim-css3-syntax',
    'mileszs/ack.vim',
    'mhinz/vim-signify',
    'othree/html5.vim',
    'pangloss/vim-javascript',
    'plasticboy/vim-markdown',
    'rodjek/vim-puppet',
    'scrooloose/nerdcommenter',
    'scrooloose/nerdtree',
    'scrooloose/syntastic',
    'techlivezheng/vim-plugin-minibufexpl',
    'timcharper/textile.vim',
    'tpope/vim-bundler',
    'tpope/vim-cucumber',
    'tpope/vim-endwise',
    'tpope/vim-fugitive',
    'tpope/vim-eunuch',
    'tpope/vim-haml',
    'tpope/vim-pathogen',
    'tpope/vim-rails',
    'tpope/vim-rake',
    'tpope/vim-commentary',
    'tpope/vim-projectionist',
    'tpope/vim-surround',
    'tpope/vim-unimpaired',
    'tpope/vim-dispatch',
    'rking/vim-joy',
    'vim-scripts/vim-coffee-script',
    'mustache/vim-mustache-handlebars',
    'killphi/vim-ruby-refactoring',
    'majutsushi/tagbar',
    'Keithbsmiley/rspec.vim',
    'slim-template/vim-slim',
    'jtratner/vim-flavored-markdown',
    'christoomey/vim-conflicted',
    'jeffkreeftmeijer/vim-numbertoggle',
    'romainl/Apprentice',
    'psf/black',
    'cespare/vim-toml',
    'vim-ruby/vim-ruby'
  ]

  each($repos) |String $repo_shortname| {
    $repo_item_dir = split($repo_shortname, '/')[1]
    $dst = "/home/$id/.vim/bundle/$repo_item_dir"
    $url = "https://github.com/${repo_shortname}"

    vcsrepo { "$dst":
      ensure   => present,
      depth    =>  1,
      provider => git,
      source   => "$url",
    }
  }

  #vcsrepo { "rking/vim-detailed":
  vcsrepo { "/home/$id/.vim/bundle/vim-detailed":
    ensure   => present,
    provider => git,
    depth    =>  1,
    source   => "https://github.com/e2/vim-detailed",
    revision => 'interpolation_indent_workaround',
  }
}
