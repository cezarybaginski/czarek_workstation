class powerline_fonts {

  $home = "/home/$id"
  $font_dirname = '.local/share/fonts'
  $cache_dir = "$home/.cache/git"

  $fc_cache = '/usr/bin/fc-cache'

  $default_font="Inconsolata-dz for Powerline.otf"
  $default_font_subdir="InconsolataDz"
  #$repo_shortname = "Lokaltog/powerline-fonts"
  $repo_shortname = "powerline/fonts"
  $repo_url = "https://github.com/${repo_shortname}.git"

  $font_dir = "$home/$font_dirname"

  $repo_item_dir = split($repo_shortname, '/')[0]
  $repo_download_base = "$cache_dir/$repo_item_dir" #/home/me/.cache/git/powerline

  $src_font_path = "${repo_download_base}/${default_font_subdir}/${default_font}"
  #$dst_font_path = "${font_dir}/${default_font}"

  vcsrepo { "${repo_download_base}" :
    #ensure  => latest,
    ensure   => present,
    provider => git,
    depth    => 1,
    source   => $repo_url,
  }

  ->

  exec { "fonts_install" :
    cwd     =>  "${repo_download_base}",
    command => "${repo_download_base}/install.sh",
    notify  => Exec['fontcache_update'],
  }

  exec { 'fontcache_update':
    refreshonly => true,
    command => "$fc_cache -v ${font_dir}"
  }
}
